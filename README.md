# Basic Rails

A basic set-up for Rails applications. Hopefully it will decrease the initial set-up time.

Original idea from [Robert Heaton](http://robertheaton.com/2013/02/13/how-to-save-1-day-a-year/).

## Includes

- [Devise](https://github.com/plataformatec/devise)
- [CanCan](https://github.com/ryanb/cancan)
- [Bootstrap](https://github.com/seyhunak/twitter-bootstrap-rails)
- [BetterErrors](https://github.com/charliesome/better_errors)
- [RSpec](https://github.com/rspec/rspec)
- [Capybara](https://github.com/jnicklas/capybara)
- [Capybara-Webkit](https://github.com/thoughtbot/capybara-webkit)
- [DatabaseCleaner](https://github.com/bmabey/database_cleaner)

## Dependencies

Capybara-Webkit depends upon a WebKit implementation from Qt. You can use [Homebrew](http://brew.sh) to install it.

    brew update
    brew install qt